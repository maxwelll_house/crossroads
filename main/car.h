#pragma once

#include <string>

using namespace std;

struct sCar {
	sCar() {
		speed = 1;
	};

	void move();

	sRect getFuturePos();
	bool needPassOtherCar(sCar* otherCar) const;

	int getFuel() const;
	virtual void refill(int& count) = 0;

	sRect rect = sRect();
	eDirection dir;
	int speed;
	int resource;
	int fuel;
	int charge;
};

struct sSimpleCar : sCar {
	sSimpleCar() {};
	sSimpleCar(const int& typeNum) {
		if (typeNum % 3 == 0) {
			resource = fuel;
			type = "gasCar";
		}
		else if (typeNum % 3 == 1) {
			resource = charge;
			type = "electroCar";
		}
	}
	void refill(int& count);
	void move();

	string type;
};

struct sHybrid : sSimpleCar {
	sHybrid() {
		resource = charge + fuel;
	};
	void refill(int& count);
	void move();
};