#include "stdafx.h"
#include "crossroad.h"
#include "car.h"

using namespace std;
/*
Программа решает очередность проезда машин на перекрестке :
● Если есть помеха справа то машина должна пропустить машину
справа.
● Если на перекрестке стоят машины со всех 4х сторон, то первой
проезжает машина, у которой минимальная координата по X.
● Машины не должны наезжать друг на друга.
*/

vector<sCar*> asdasd;
const int initialCarsCount = 10;

#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768

void spawnCarFromTop(sCar* car);
void spawnCarFromBot(sCar* car);
void spawnCarFromLeft(sCar* car);
void spawnCarFromRight(sCar* car);
void spawnCar();
bool main_loop();

int main(int argc, char** argv) {
    for (auto i = 0; i < initialCarsCount; ++i) {
        spawnCar();
    }
    main_loop();
    return 0;
}


bool main_loop() {
    for (auto car : asdasd) {
        for (auto car22 : asdasd) {
            if (car->getFuturePos().intersects(car22->getFuturePos())) {
                if (car->needPassOtherCar(car22)) {
                    car->move();
                }
            }
            else {
                car22->move();
            }
        }
        if (car->rect.pos.x <= 0 || car->rect.pos.x >= SCREEN_WIDTH || car->rect.pos.y <= 0 || car->rect.pos.y >= SCREEN_HEIGHT)
            spawnCar();
    }
    return main_loop();
}

void spawnCarFromTop(sCar* car) {
    car->rect = sRect(SCREEN_WIDTH / 2, 0, 100, 100);
    car->dir = eDirection::DOWN;
}

void spawnCarFromBot(sCar* car) {
    car->rect = sRect(SCREEN_WIDTH / 2, SCREEN_HEIGHT, 100, 100);
    car->dir = eDirection::UP;
}

void spawnCarFromLeft(sCar* car) {
    car->rect = sRect(0, SCREEN_HEIGHT / 2, 100, 100);
    car->dir = eDirection::RIGHT;
}

void spawnCarFromRight(sCar* car) {
    car->rect = sRect(0, SCREEN_HEIGHT / 2, 100, 100);
    car->dir = eDirection::LEFT;
}

void spawnCar() {
    sCar* car;
    int carType = rand();
    int direct = rand();
    if (carType % 3 == 0 || carType % 3 == 1) {
        car = new sSimpleCar(carType);
    }
    else {
        car = new sHybrid();
    }
    if (direct % 4 == 1)
        spawnCarFromRight(car);
    else if (direct % 4 == 2)
        spawnCarFromTop(car);
    else if (direct % 4 == 3)
        spawnCarFromBot(car);
    else if (direct % 4 == 4)
        spawnCarFromLeft(car);
    asdasd.push_back(car);
}

bool sRect::intersects(const sRect& other) const {
    return !((other.pos.x + other.size.width <= pos.x) ||
        (other.pos.y + other.size.height <= pos.y) ||
        (other.pos.x >= pos.x + size.width) ||
        (other.pos.y >= pos.y + size.height));
}


void sCar::move() {
    switch (dir) {
    case eDirection::UP:
        rect.pos.y += speed;
    case eDirection::DOWN:
        rect.pos.y -= speed;
    case eDirection::RIGHT:
        rect.pos.x += speed;
    case eDirection::LEFT:
        rect.pos.x -= speed;
    }
}

sRect sCar::getFuturePos() {
    switch (dir) {
    case eDirection::UP:
        return sRect(rect.pos.x, rect.pos.y + speed, rect.size.width, rect.size.height);
    case eDirection::DOWN:
        return sRect(rect.pos.x, rect.pos.y - speed, rect.size.width, rect.size.height);
    case eDirection::RIGHT:
        return sRect(rect.pos.x + speed, rect.pos.y, rect.size.width, rect.size.height);
    case eDirection::LEFT:
        return sRect(rect.pos.x - speed, rect.pos.y, rect.size.width, rect.size.height);
    }
}

bool sCar::needPassOtherCar(sCar* otherCar) const {
    eDirection otherdir = otherCar->dir;
    eDirection dir = this->dir;
    bool result = false;
    switch (dir) {
    case eDirection::UP:
        if (otherdir == eDirection::LEFT) {
            result = true;
            break;
        }
    case eDirection::DOWN:
        if (otherdir == eDirection::RIGHT) {
            result = true;
            break;
        }
    case eDirection::RIGHT:
        if (otherdir == eDirection::UP) {
            result = true;
            break;
        }
    case eDirection::LEFT:
        if (otherdir == eDirection::DOWN) {
            result = true;
            break;
        }
        else {
            result = false;
            break;
        }
    }
    return result;
}

int sCar::getFuel() const {
    return resource;
};


void sSimpleCar::refill(int& count) {
    resource += count;
}
void sSimpleCar::move() {
    resource--;
    sCar::move();
}


void sHybrid::refill(int& count) {
    charge += count / 2;
    fuel += count / 2;
}
void sHybrid::move() {
    int res = rand();
    if (res % 2 == 0)
        charge--;
    else
        fuel--;
    sCar::move();
}




